/**
 * @file
 * Disclaimer popup.
 */
(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.node_disclaimer = {
    attach: function (context, settings) {
      $.each(drupalSettings.disclaimer, function (index, value) {
        // Show disclaimer if we dont value saved cookie value for current node.
        if ($.cookie(index) !== '1') {
          $('<div>', context).dialog({
            closeOnEscape: false,
            open: function (event, ui) {
              $('.ui-dialog-titlebar-close', ui.dialog | ui).hide();
              $('.ui-dialog-content').html(value.text);
            },
            resizable: false,
            height: 'auto',
            width: '40%',
            modal: true,
            buttons: {
              'accept': {
                text: 'OK',
                click: function () {
                  $(this).dialog('close');
                  var expire = new Date(new Date().getTime() + 31556926);
                  $.cookie(index, '1', {expires: expire});
                }
              },
            }
          });
        }


      });
    }
  };

})(jQuery, Drupal, drupalSettings);

